[Home](home.md) | [PROJECT](PROJECT.md) | [team](team.md) | [vocabulary](terminologies.md)
1. Arduino UNO or compatible board: A microcontroller board for prototyping digital devices.

2. USB cable: Used to connect the Arduino board to a computer for programming and power.

3. Full-sized breadboard: A solderless board for building electronic circuits.

4. Solid jumper wire kit: Wires used to connect components on the breadboard.

5. Male-male flexible jumper wires: Flexible wires for connecting components.

6. Alligator clip leads: Clips used for temporary connections.

7. 10kΩ potentiometers: Adjustable resistors for voltage control.

8. 1/4W 220Ω resistors: Resistors limiting electric current.

9. 1/4W 4.7kΩ resistors: Resistors with higher resistance.

10. Optional: 1/4W resistor kit: Kit with various resistor values.

11. Diffused 5mm green LEDs: Green light-emitting diodes.

12. Diffused 5mm blue LEDs: Blue light-emitting diodes.

13. Optional: LED kit: Kit with various LED colors.

14. Conductive rubber cord: Flexible cord for conducting electricity.






[Home](home.md) | [PROJECT](PROJECT.md) | [team](team.md) | [vocabulary](terminologies.md)

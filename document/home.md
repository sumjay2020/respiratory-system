[Home](home.md) | [PROJECT](PROJECT.md) | [team](team.md) | [vocabulary](terminologies.md)
# Welcome to our Respiratory Project!
![](images/home.jpg)



#### About Us:
 Welcome to Lung Logic Innovations! We're a team of four students driven by curiosity and a passion for understanding the respiratory system better. Our project focuses on creating a working model of the respiratory system to explore its complexities and implications for the environment and human health. Meet our team: Sumjay, Kinley Sonam, Rinzin Wangmo, and Tenzin Rabgay Zangmo. Together, we're blending technology and nature-inspired innovation to unravel the mysteries of breathing.

 ## Inspiration     
 Our idea to create a respiratory system with a breathing monitor was sparked by our struggles in understanding how breathing works when we were in 10th grade. We remember how confusing it was, and we want to make it easier for others to understand. By using technology and our own experiences, we hope to simplify the concept and help others see how amazing the respiratory system really is.


[Home](home.md) | [PROJECT](PROJECT.md) | [team](team.md) | [vocabulary](terminologies.md)
 
[Home](home.md) | [PROJECT](PROJECT.md) | [team](team.md) | [vocabulary](terminologies.md)

![](images/gru.jpg)

## SUMJAY
![](images/sumjay.jpg)

My name is Sumjay, and I'm currently in grade 11.  I did the documentaton of our project. I am very happy to work with my teamates. Collaborating with my friends, we've developed a respiratory system working model project aimed at providing students with a comprehensive understanding of this vital physiological process. Through hands-on interaction with our model, learners will explore the intricacies of respiration, from the mechanics of breathing to gas exchange in the lungs. By integrating elements from various disciplines such as biology, anatomy, and engineering, our project fosters interdisciplinary learning, offering a holistic approach to understanding the respiratory system. We're excited about the educational opportunities our project presents and its potential to inspire curiosity and deeper engagement among our peers.



## Kinley Sonam
![](images/kin.jpg)
My name is Kinley Sonam and I am in grade 11. I am from Thimphu and I live in Changtagang. Me and my friends created a projected on respiratory system that would help the younger ones to understand and grab the concepts related to respiratory. I hope you enjoy it!!

## Rinzin
![](images/rinzin.jpg)
- Age: 18
- Hobbies: Reading, Singing
- What I hope to learn : I'm eager to dive into Arduino programming and integrate technology into environmental projects. Plus, I'm looking forward to refining my teamwork and communication skills along the way.
- Contribution to the team: In the team, I want to share my excitement about Arduino and environmental projects. I'll join discussions, offer ideas, and work well with everyone. Plus, I'll use my communication skills to keep things running smoothly.

## Tenzin
![](images/ten.jpg)
- Age: 16
- Hobbies: Reading, Writing, Baking
- What I hope to learn: I'm looking forward to learning how to use Arduino to make cool gadgets    for the environment. I'm also eager to deepen my understanding of how the respiratory system functions and how environmental factors influence it.
- Contribution To the Team: I'm excited to contribute my creative ideas to the team. When we encounter challenges, I'll brainstorm fresh approaches to solve them. Together, we'll discover some awesome solutions!

#### PEER learning :
In our group, we've strategically divided the workload amongst ourselves, recognizing the importance of not only completing our individual tasks but also fostering a comprehensive understanding of each other's contributions. To achieve this, we've devised a plan to regularly engage in knowledge-sharing sessions after completing our respective assignments. Through these sessions, we aim to provide insights into our own work processes, methodologies, and outcomes, thereby enabling our teammates to gain valuable insights and learn from our experiences. This collaborative approach not only strengthens our collective knowledge base but also cultivates a supportive and collaborative environment within our team.






[Home](home.md) | [PROJECT](PROJECT.md) | [team](team.md) | [vocabulary](terminologies.md)
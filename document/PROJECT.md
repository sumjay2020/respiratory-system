[Home](home.md) | [PROJECT](PROJECT.md) | [team](team.md) | [vocabulary](terminologies.md)

# Respiratory System


![](images/res.jpg)

#### We are developing a functional model of the Respiratory system with the primary objective of enhancing students' comprehension of our respiratory system through various interdisciplinary domains. Our approach involves integrating multiple domains to provide a comprehensive understanding of the respiratory system.
![](./images/image.jpg)

# Working Concept :
![](./images/p.jpg)
 The biomimetic respiratory system incorporates a breathing pump inspired by the Calvin cycle to facilitate gas exchange, operating cyclically to simulate inhalation and exhalation while minimizing energy usage. Integrated with a breathing monitor equipped with sensors for respiratory parameters, the system continuously tracks breathing patterns and oxygen levels, providing real-time feedback on respiratory performance. Coordinated by a control system, the pump's operation is adjusted based on monitored data, ensuring optimal gas exchange rates and enabling applications in healthcare, environmental monitoring, and robotics, with potential uses ranging from respiratory therapy to air quality measurement and autonomous navigation in oxygen-varying environments. Powered by batteries or a stable power source, the system offers versatility and functionality for various applications, showcasing the fusion of biomimicry and modern technology in respiratory solutions.
![]

## Research : 
This research looks at making models that copy how we breathe and can also keep track of how well we're breathing in real time. We're trying to make it easy for people to understand and also helpful for doctors to check our breathing. We're testing these models to see how well they work and if they could be useful for teaching or for people to use at home to keep an eye on their breathing. In the future, we want to make them even better and find more ways they can help in hospitals.
# Components required :
1. Arduino UNO or compatible board

2. USB cable (type depends on board)

3. Full-sized breadboard
Solid jumper wire kit

4. Male-male flexible jumper wires

5. Alligator clip leads

6. 10kΩ potentiometers (breadboard compatible)

7. 1/4W 220Ω resistors

8. 1/4W 4.7kΩ resistors

9. Optional: 1/4W resistor kit (if you do not want to purchase resistors individually)

10. Diffused 5mm green LEDs (or other color of your choice)

11. Diffused 5mm blue LEDs (or other color of your choice)

12. Optional: LED kit (if you do not want to purchase LEDs individually)

13. Conductive rubber cord

[Home](home.md) | [PROJECT](PROJECT.md) | [team](team.md) | [vocabulary](terminologies.md)